﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.Interfaces;

namespace Zaroda.DAO.Objects
{
    class Car : ICar
    {
        private static int iterator = 1;

        public Car() {
            this.ID = ++Car.iterator;
            this.ProductionYear = DateTime.Now.Year;
        }

        public Car(string Name, int ProductionYear, IProducer producer)
        {
            this.ID = ++Car.iterator;
            this.Name = Name;
            this.ProductionYear = ProductionYear;
            this.Sold = false;
            this.Producer = producer;
        }

        public override string ToString()
        {
            return this.Producer.Name + " "+ this.Name + " " + this.ProductionYear;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public int ProductionYear { get; set; }
        public bool Sold { get; set; }
        public IProducer Producer { get; set; }
    }
}
