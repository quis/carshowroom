﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.Interfaces;

namespace Zaroda.DAO.Objects
{
    class Producer : IProducer
    {
        private static int iterator = 1;

        public Producer(string Name)
        {
            this.ID = ++Producer.iterator;
            this.Name = Name;
        }

        public override string ToString()
        {
            return this.Name;
        }
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
