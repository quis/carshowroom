﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.Interfaces;
using Zaroda.DAO.Objects;

namespace Zaroda.DAO
{
    public class DAOMock : IDAO
    {
        private List<ICar> cars;
        private List<IProducer> producers;

        public DAOMock()
        {
            IProducer fiat = new Producer("Fiat");
            IProducer opel = new Producer("Opel");

            cars = new List<ICar>() {
                new Car("Punto", 2005, fiat),
                new Car("Corsa", 2010, opel),
            };

            producers = new List<IProducer>() {
                fiat,
                opel
            };
        }

        public IEnumerable<ICar> GetCarsCollection()
        {
            return cars;
        }

        public IEnumerable<IProducer> GetProducersCollection()
        {
            return producers;
        }

        public IProducer CreateProducer()
        {
            return new Producer("");
        }

        public ICar CreateCar()
        {
            return new Car();
        }

        public void DeleteCar(ICar car)
        {
            cars.Remove(car);
        }

        public void DeleteProducer(IProducer producer)
        {
            producers.Remove(producer);
        }
    }
}
