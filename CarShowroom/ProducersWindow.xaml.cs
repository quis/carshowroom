﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Zaroda.CarShowroom.ViewModels;

namespace Zaroda.CarShowroom
{
    /// <summary>
    /// Logika interakcji dla klasy ProducersWindow.xaml
    /// </summary>
    public partial class ProducersWindow : Window
    {
        public ProducersWindow()
        {
            this.DataContext = new ProducersList();
            InitializeComponent();
        }
    }
}
