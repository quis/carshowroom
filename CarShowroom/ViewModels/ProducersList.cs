﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.CarShowroom.Commands;
using Zaroda.Interfaces;

namespace Zaroda.CarShowroom.ViewModels
{
    class ProducersList : ViewModel
    {
        private ObservableCollection<Producer> producersList;
        private Producer selectedProducer;
        private BL.App app;
        private RelayCommand addProducerCommand;
        private RelayCommand deleteProducerCommand;

        public ProducersList()
        {
            app = BL.App.getInstance();
            producersList = new ObservableCollection<Producer>();
            LoadProducers();

            addProducerCommand = new RelayCommand(new Action<object>(this.AddProducerCommandExecute));
            deleteProducerCommand = new RelayCommand(new Action<object>(this.DeleteProducerCommandExecute));
        }

        private void LoadProducers()
        {
            foreach (IProducer producer in app.GetProducers())
            {
                Producers.Add(new Producer(producer));
            }
        }

        private void AddProducerCommandExecute(object o)
        {
            IProducer producer = app.CreateProducer();
            Producers.Add(new Producer(producer));
        }

        private void DeleteProducerCommandExecute(object o)
        {
            Producer producer = (Producer)o;
            Producers.Remove(producer);
            app.DeleteProducer(producer.ProducerData);
        }

        public Producer SelectedProducer
        {
            get
            {
                return selectedProducer;
            }
            set
            {
                selectedProducer = value;
                OnPropertyChanged(nameof(SelectedProducer));
            }
        }

        public ObservableCollection<Producer> Producers
        {
            get
            {
                return producersList;
            }
            set
            {
                producersList = value;
                OnPropertyChanged(nameof(Producers));
            }
        }

        public RelayCommand AddProducerCommand
        {
            get
            {
                return addProducerCommand;
            }
        }

        public RelayCommand DeleteProducerCommand
        {
            get
            {
                return deleteProducerCommand;
            }
        }
    }
}
