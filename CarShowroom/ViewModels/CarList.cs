﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.CarShowroom.Commands;
using Zaroda.Interfaces;

namespace Zaroda.CarShowroom.ViewModels
{
    class CarList : ViewModel
    {
        private ObservableCollection<Car> carList;
        private Car selectedCar;
        private BL.App app;
        private RelayCommand addNewCarCommand;
        private RelayCommand editCarCommand;
        private RelayCommand deleteCarCommand;
        private RelayCommand soldCarCommand;

        public CarList()
        {
            app = BL.App.getInstance();
            carList = new ObservableCollection<Car>();
            LoadCars();

            addNewCarCommand = new RelayCommand(new Action<object>(this.AddCarCommandExecute));
            editCarCommand = new RelayCommand(new Action<object>(this.EditCarCommandExecute), new Predicate<object>(this.IsCarSelectedAndNotSold));
            deleteCarCommand = new RelayCommand(new Action<object>(this.DeleteCarCommandExecute), new Predicate<object>(this.IsCarSelected));
            soldCarCommand = new RelayCommand(new Action<object>(this.SoldCarCommandExecute), new Predicate<object>(this.IsCarSelectedAndNotSold));
        }

        private void LoadCars()
        {
            foreach(ICar car in app.GetCars())
            {
                Cars.Add(new Car(car, false, this));
            }
        }

        private void AddCarCommandExecute(object o)
        {
            CarEditWindow carEditWindow = new CarEditWindow();
            Car car = new Car(app.CreateCar(), true, this);
            car.EditWindow = carEditWindow;
            carEditWindow.DataContext = car;
            carEditWindow.Show();
        }

        private void EditCarCommandExecute(object o)
        {
            CarEditWindow carEditWindow = new CarEditWindow();
            selectedCar.EditWindow = carEditWindow;
            carEditWindow.DataContext = selectedCar;
            carEditWindow.Show();
        }

        private void DeleteCarCommandExecute(object o)
        {
            app.DeleteCar(selectedCar.CarData);
            Cars.Remove(selectedCar);
        }

        private void SoldCarCommandExecute(object o)
        {
            selectedCar.Sold = true;
        }

        private bool IsCarSelectedAndNotSold(object o)
        {
            return this.IsCarSelected(o) && !selectedCar.Sold;
        }

        private bool IsCarSelected(object o)
        {
            return selectedCar != null; 
        }

        public void UpdateList()
        {
            OnPropertyChanged(nameof(Cars));
        }
        
        public ObservableCollection<Car> Cars
        {
            get
            {
                return carList;
            }
            set
            {
                carList = value;
                OnPropertyChanged(nameof(Cars));
            }
        }

        public Car SelectedCar
        {
            get
            {
                return selectedCar;
            }
            set
            {
                selectedCar = value;
                OnPropertyChanged(nameof(SelectedCar));
            }
        }

        public RelayCommand AddNewCarCommand
        {
            get
            {
                return addNewCarCommand;
            }
        }

        public RelayCommand EditCarCommand
        {
            get
            {
                return editCarCommand;
            }
        }

        public RelayCommand DeleteCarCommand
        {
            get
            {
                return deleteCarCommand;
            }
        }

        public RelayCommand SoldCarCommand
        {
            get
            {
                return soldCarCommand;
            }
        }
    }
}
