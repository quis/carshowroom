﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.CarShowroom.Commands;
using Zaroda.Interfaces;

namespace Zaroda.CarShowroom.ViewModels
{
    class Car : ViewModel
    {
        private BL.App app;
        private RelayCommand saveCommand;
        private RelayCommand cancelCommand;
        private CarEditWindow editWindow;
        protected ICar car;
        private bool isNew;
        private CarList parentList;

        public Car(ICar car, bool isNew, CarList parentList)
        {
            app = BL.App.getInstance();
            this.car = car;
            this.isNew = isNew;
            this.parentList = parentList;

            saveCommand = new RelayCommand(new Action<object>(this.SaveCommandExecute), new Predicate<object>(this.SaveCommandCanExecute));
            cancelCommand = new RelayCommand(new Action<object>(this.CancelCommandExecute));
        }

        public override string ToString()
        {
            return this.car.ToString();
        }

        private void CancelCommandExecute(object o)
        {
            EditWindow.Close();
        }

        private void SaveCommandExecute(object o)
        {
            if(isNew)
            {
                this.parentList.Cars.Add(this);
            }
            this.parentList.UpdateList();
            EditWindow.Close();
        }

        private bool SaveCommandCanExecute(object o)
        {
            return Name?.Length > 0 && ProductionYear > 1800 && ProductionYear <= DateTime.Now.Year;
        }

        public RelayCommand SaveCommand
        {
            get
            {
                return saveCommand;
            }
        }
        public RelayCommand CancelCommand
        {
            get
            {
                return cancelCommand;
            }
        }

        public CarEditWindow EditWindow
        {
            get => editWindow;
            set
            {
                editWindow = value;
            }
        }

        public IEnumerable<IProducer> Producers
        {
            get
            {
                return app.GetProducers();
            }
        }

        public ICar CarData
        {
            get => car;
        }

        public int ID
        {
            get => car.ID;
            set
            {
                car.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }

        public string Name
        {
            get => car.Name;
            set
            {
                car.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int ProductionYear
        {
            get => car.ProductionYear;
            set
            {
                car.ProductionYear = value;
                OnPropertyChanged(nameof(ProductionYear));
            }
        }

        public IProducer Producer
        {
            get => car.Producer;
            set
            {
                car.Producer = value;
                OnPropertyChanged(nameof(Producer));
            }
        }

        public bool Sold
        {
            get => car.Sold;
            set
            {
                car.Sold = value;
                OnPropertyChanged(nameof(Sold));
                OnPropertyChanged(nameof(SoldFormatted));
            }
        }

        public string SoldFormatted
        {
            get
            {
                return car.Sold ? "Tak" : "Nie";
            }
        }
    }
}
