﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.Interfaces;

namespace Zaroda.CarShowroom.ViewModels
{
    class Producer : ViewModel
    {
        private IProducer producer;

        public Producer(IProducer producer)
        {
            this.producer = producer;
        }

        public IProducer ProducerData
        {
            get => producer;
        }

        public int ID
        {
            get => producer.ID;
            set
            {
                producer.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }

        public string Name
        {
            get => producer.Name;
            set
            {
                producer.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
    }
}