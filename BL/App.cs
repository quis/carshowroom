﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaroda.DAO;
using Zaroda.Interfaces;

namespace Zaroda.BL
{
    public class App
    {
        private static BL.App instance;

        public static BL.App getInstance()
        {
            if (instance == null)
            {
                instance = new BL.App();
            }
            return instance;
        }

        private IDAO data;

        private App()
        {
            data = new DAOMock();
        }

        public IEnumerable<ICar> GetCars()
        {
            return data.GetCarsCollection();
        }

        public IEnumerable<IProducer> GetProducers()
        {
            return data.GetProducersCollection();
        }

        public ICar CreateCar()
        {
            return data.CreateCar();
        }

        public IProducer CreateProducer()
        {
            return data.CreateProducer();
        }

        public void DeleteProducer(IProducer producer)
        {
            data.DeleteProducer(producer);
        }

        public void DeleteCar(ICar car)
        {
            data.DeleteCar(car);
        }
    }
}
