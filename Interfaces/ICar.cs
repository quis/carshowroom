﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaroda.Interfaces
{
    public interface ICar
    {
        int ID { get; set; }
        string Name { get; set; }
        int ProductionYear { get; set; }
        bool Sold { get; set; }
        IProducer Producer { get; set; }
    }
}
