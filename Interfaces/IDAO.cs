﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaroda.Interfaces
{
    public interface IDAO
    {
        IEnumerable<ICar> GetCarsCollection();
        IEnumerable<IProducer> GetProducersCollection();
        ICar CreateCar();
        IProducer CreateProducer();
        void DeleteCar(ICar car);
        void DeleteProducer(IProducer car);
    }
}
